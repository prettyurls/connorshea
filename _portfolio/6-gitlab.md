---
title: "GitLab"
image: "gitlab/gitlab-logo.svg"
thumbnail: "gitlab/gitlab-thumbnail.svg"
role: "Frontend Engineer"
description: "GitLab is a full software development toolkit, complete with issues, code review, source control, and CI – and Community Edition is entirely open source. After contributing regularly for a few months in early 2016, I applied and was accepted for a Frontend Engineering internship. I've worked on frontend performance and security, implementing smaller features and improvements, and upgrading the application to Rails 5."
link: "https://www.about.gitlab.com"
date_range: "May 2016 — Present"
---
