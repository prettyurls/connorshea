# connorshea.github.io
This is my personal website and blog, built with [Jekyll](http://jekyllrb.com/).

## License

See [LICENSE](LICENSE).
