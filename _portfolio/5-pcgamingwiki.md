---
title: "PCGamingWiki Redesign"
image: "pcgamingwiki/pcgamingwiki-logo.svg"
thumbnail: "pcgamingwiki/pcgamingwiki-thumbnail.svg"
role: "Lead Web Developer/Designer"
description: "We redesigned 'The wiki about fixing PC games' to better adapt to mobile devices, load faster, and improve the overall experience of our users."
link: "http://pcgamingwiki.com"
date_range: "February — August 2015"
---
