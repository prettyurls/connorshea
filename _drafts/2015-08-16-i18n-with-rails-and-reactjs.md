---
layout: post
title: Internationalization (i18n) with Ruby on Rails and React.js
tags: [rails, i18n, react]
---

{% include image.html url="/images/posts/i18n-with-rails-and-reactjs/react-ruby.svg" %}

In working on [Fiction Dock][fiction-dock], we ran into the problem of using Rails' [i18n][i18n-gem] gem with [React.js][reactjs]. While solutions like Yahoo's [React Intl][react-intl] exist, they didn't really suit our needs. We'd already invested quite a bit of time and effort into organizing our localization strings into YAML files as is recommended by the i18n gem.

For example, a snippet from our `stories.en.yml` file:

{% highlight yaml %}
en:
  stories:
    form:
      story_name: "Story name"
      language: "Language"
      license: "License"
      blurb: "Blurb"
      write_a_very_short_description_of_the_story: "Give a brief synopsis of the story"
      description: "Description"
      write_a_description_of_the_story: "Write a description of the story"
      franchises: "Franchises"
      franchise: "Franchise"
      add_a_new_franchise: "Add a new franchise"
      characters: "Characters"
      add_characters: "Add characters"
      ships: "Ships"
      add_a_new_ship: "Add a new ship"
      no_suggestions_found: "No suggestions found"
      remove: "Remove"
      submit: 
        new: "Submit"
        edit: "Save changes"
{% endhighlight %}

The benefits of using this format are many. Localized strings for each section of the site (e.g. bookshelves, stories, franchises, characters, and frontpage) are available in a single file, keeping things from becoming complex while also avoiding a single monolithic strings file. With a single place for all strings in the site, copy-editing can be done quickly without having to scan through dozens of HTML and JavaScript files.

Sometimes strings are even included directly in models, for example with mailers or notifications. Having everything in a centralized location is huge. While we have yet to translate the site, this will also pay dividends should we choose to do so, or should someone volunteer their time and contribute.


### How we usually handle i18n strings

Normally we would use the Rails helpers provided by the i18n gem, like so:

{% highlight erb %}
<li><%= t(".author") %></li>
{% endhighlight %}

Moving text strings out of your HTML files is vital to getting a web application ready for localization, and `i18n` makes this separation very simple.

i18n strings don't have to be static. They can be generated dynamically based on page content. For example, the following is based on the current story's `language` variable:

{% highlight erb %}
<li><%= t("languages.#{@story.language}") %></li>
{% endhighlight %}



### The Problem





[fiction-dock]: http://www.fictiondock.com
[i18n-gem]:     https://rubygems.org/gems/i18n
[reactjs]:      http://facebook.github.io/react
[react-intl]:   http://formatjs.io/react/
