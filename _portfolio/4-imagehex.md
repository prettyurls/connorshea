---
title: "ImageHex"
image: "imagehex/imagehex-logo.svg"
thumbnail: "imagehex/imagehex-thumbnail.svg"
role: "Lead Front-End Developer"
description: "ImageHex is a place to search, share, and commission images and other artwork. I've lead the design and front-end development of the site since its inception."
link: "http://www.imagehex.com"
date_range: "July 2014 — March 2016"
---
